package com.ferbey;

import java.util.Random;

/**
 * Represents an Main.
 *
 * @author Ferbey Daria
 * @version 6
 * @since 2019-11-19
 */
public class User {
    /**
     * name of users.
     */
    private String name;
    /**
     * lastName of users.
     */
    private String lastName;

    /**
     * Show message.
     *
     * @return message
     */
    public final String showMeMessage() {
        return "This is some message";
    }

    /**
     * Create random integer number.
     *
     * @return random integer number.
     */
    public final Integer giveMeASign() {
        Random random = new Random();
        return random.nextInt();
    }

    /**
     * Get name user.
     *
     * @return name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Set variable name.
     *
     * @param n user`s name
     */
    public final void setName(final String n) {
        this.name = n;
    }

    /**
     * Get last name user.
     *
     * @return variable lastName.
     */
    public final String getLastName() {
        return lastName;
    }

    /**
     * Set lastName.
     *
     * @param surname lastName of user.
     */
    public final void setLastName(final String surname) {
        this.lastName = surname;
    }
}
