package com.ferbey;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Represents an Main.
 *
 * @author Ferbey Daria
 * @version 6
 * @since 2019-11-19
 */
public class Main {
    /**
     * Logger for logging.
     */
    static final Logger ROOTLOOGER = LogManager.getRootLogger();
    /**
     * Logger for logging.
     */
    static final Logger USERLOGGER = LogManager.getLogger(User.class);

    /**
     * Default constructor.
     */
    public Main() {
    }

    /**
     * Practice logging.
     *
     * @param args unused.
     */
    public static void main(final String[] args) {
        User user = new User();
        user.setName("Anakin");
        user.setLastName("Skywalker");

        USERLOGGER.info(user.showMeMessage());
        USERLOGGER.info(user.giveMeASign());

        ROOTLOOGER.info("Root Logger: " + user.showMeMessage());

        //debug
        if (ROOTLOOGER.isDebugEnabled()) {
            ROOTLOOGER.debug("RootLogger: In debug message");
            USERLOGGER.debug("UserLogger in debug");
        }

        try {
            User userNull = new User();
            userNull.getName().toString();
        } catch (NullPointerException ex) {
            USERLOGGER.error("error message: " + ex.getMessage());
            USERLOGGER.fatal("fatal error message: " + ex.getMessage());
        }

    }
}
